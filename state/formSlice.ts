import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'
import { FormType } from '@/models/FormModel'

interface FormState {
  formReducer: FormType
  mode: string
  user: object | null
  token: object | null
  posts: object[]
}

const initialState: FormState = {
  formReducer: {
    title: 'Dévéloppeur web - WeaveLines',
    description:
      'Nous vous remercions de votre intérêt pour cette opportunité.',
    form: [],
  },
  mode: 'light',
  user: null,
  token: null,
  posts: [],
}

export const formSlice = createSlice({
  name: 'formBuilder',
  initialState,
  reducers: {
    updateTitle: (state, action) => {
      state.formReducer.title = action.payload
    },
    updateDescription: (state, action) => {
      state.formReducer.description = action.payload
    },
    addToform: (state, action) => {
      state.formReducer.form?.push(action.payload)
    },
    removeToform: (state, action: PayloadAction<string>) => {
      const formFilter = state.formReducer.form?.filter(
        (item) => item.id !== action.payload
      )
      state.formReducer.form = formFilter
    },
    toggleToform: (state, action) => {
      const updatedForm = state.formReducer.form?.map((el) => {
        if (el.id === action.payload.id) return action.payload
        return el
      })
      state.formReducer.form = updatedForm
    },

    updateFormElement: (state, action) => {
      const updatedForm = state.formReducer.form?.map((el) => {
        if (el.id === action.payload.id) return action.payload
        return el
      })
      state.formReducer.form = updatedForm
    },
    //options array
    addInformOption: (state, action) => {
      state.formReducer.form?.map((el) => {
        if (el.id === action.payload.id) {
          return el.options?.push(action.payload.body)
        } else {
          return el
        }
      })
    },
    removeInformOption: (state, action) => {
      state.formReducer.form?.map((el) => {
        if (el.id === action.payload.id) {
          return el.options?.splice(action.payload.indexOption, 1)
        } else {
          return el
        }
      })
    },
    updateOptionement: (state, action) => {
      state.formReducer.form?.map((el) => {
        if (el.id === action.payload.id) {
          return el.options?.splice(
            action.payload.indexOption,
            1,
            action.payload.body
          )
        } else {
          return el
        }
      })
    },

    //reset form
    cleareForm: (state) => {
      state.formReducer.form = []
    },
  },
})

export const {
  updateTitle,
  updateDescription,
  addToform,
  removeToform,
  toggleToform,
  updateFormElement,
  addInformOption,
  removeInformOption,
  updateOptionement,
  cleareForm,
} = formSlice.actions

export default formSlice.reducer
