import { useState } from 'react'
import Head from 'next/head'
import styles from '@/styles/Home.module.css'
import { Header } from '@/components/createForm/Header'
import { LeftBlock } from '@/components/createForm/LeftBlock'
import { RightBlock } from '@/components/createForm/RightBlock'
import { AiOutlineFileText } from 'react-icons/ai'

export default function Home() {
  const [toggleBtn, setToggleBtn] = useState<boolean>(false)
  return (
    <>
      <Head>
        <title>Créer un formulaire - formBuildeR</title>
        <meta name="description" content="Créer un formulaire - formBuildeR" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className={styles.main}>
        <Header />
        <div className={styles.container}>
          <LeftBlock />
          <RightBlock toggleBtn={toggleBtn} setToggleBtn={setToggleBtn} />
        </div>
        <div
          className={
            toggleBtn
              ? `${styles.edit_form} ${styles.active}`
              : styles.edit_form
          }
        >
          <button onClick={() => setToggleBtn(true)}>
            <AiOutlineFileText className={styles.icon_view} />
          </button>
        </div>
      </main>
    </>
  )
}
