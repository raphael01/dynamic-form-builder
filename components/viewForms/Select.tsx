import styles from '@/styles/viewForms/Form.module.css'
import { Form } from '@/models/FormModel'
import { FieldValues, UseFormRegister } from 'react-hook-form'

type Elements = {
  item: Form
  index: number
  register: UseFormRegister<FieldValues>
}

export const Select = ({ item, index, register }: Elements) => {
  return (
    <div>
      <label htmlFor={`${item.label} ${index}`} className={styles.label}>
        {index + 1}. {item.label} :
        {item.isRequired && <span className={styles.isRequired}>*</span>}
      </label>
      <select
        id={`${item.label} ${index}`}
        {...register(item.label, { required: item.isRequired })}
      >
        <option disabled defaultValue="Choisir une option">
          Choisir une option
        </option>
        {item.options?.map((el, i) => (
          <option value={el} key={i}>
            {el}
          </option>
        ))}
      </select>
    </div>
  )
}
