import styles from '@/styles/viewForms/Form.module.css'
import { Form } from '@/models/FormModel'
import { FieldValues, UseFormRegister } from 'react-hook-form'

type Elements = {
  item: Form
  index: number
  register: UseFormRegister<FieldValues>
}

export const CheckBox = ({ item, index, register }: Elements) => {
  return (
    <div>
      <p className={styles.label}>
        {index + 1}. {item.label} :
        {item.isRequired && <span className={styles.isRequired}>*</span>}
      </p>
      {item.options?.map((el, i) => (
        <div key={i} className={styles.checbox_btn}>
          <input
            type="checkbox"
            id={`${i}${index}`}
            {...register(item.label, { required: item.isRequired })}
            value={el}
          />
          <label htmlFor={`${i}${index}`}>{el}</label>
        </div>
      ))}
    </div>
  )
}
