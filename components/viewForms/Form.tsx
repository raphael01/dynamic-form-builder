import styles from '@/styles/viewForms/Form.module.css'
import { useForm } from 'react-hook-form'
import Link from 'next/link'
import { FormType } from '@/models/FormModel'
import { Toast } from '@/hooks/useInfosMessage'
import { CheckBox } from './CheckBox'
import { BtnRadio } from './BtnRadio'
import { Select } from './Select'
import { Input } from './Input'

type data = {
  dataForm: FormType
}

export const Form = ({ dataForm }: data) => {
  const { register, reset, handleSubmit } = useForm()
  const onSubmit = handleSubmit((data) => {
    Toast.fire({
      icon: 'success',
      title: 'Formulaire envoyer avec succès.',
    })
    reset()
    console.log(data)
  })

  return (
    <form onSubmit={onSubmit} autoComplete="off" className={styles.container}>
      {dataForm.form?.map((item, index) => (
        <div key={index} className={styles.container_block}>
          {item.html_element === 'text' ||
          item.html_element === 'email' ||
          item.html_element === 'url' ||
          item.html_element === 'date' ? (
            <Input item={item} index={index} register={register} />
          ) : null}

          {item.html_element === 'select' && (
            <Select item={item} index={index} register={register} />
          )}

          {item.html_element === 'radio' && (
            <BtnRadio item={item} index={index} register={register} />
          )}

          {item.html_element === 'checkbox' && (
            <CheckBox item={item} index={index} register={register} />
          )}
        </div>
      ))}

      <div className={styles.footer}>
        <button type="submit">Envoyer</button>
        <div>
          <p>
            N’envoyez jamais de mots de passe depuis des formulaires
            formBuildeR.
          </p>
          <p>
            <Link href="/">Signalez un abus</Link> ou consultez notre{' '}
            <Link href="/">politique de confidentialité.</Link>
          </p>
        </div>
      </div>
    </form>
  )
}
