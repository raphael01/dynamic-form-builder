import styles from '@/styles/viewForms/Form.module.css'
import { Form } from '@/models/FormModel'
import { FieldValues, UseFormRegister } from 'react-hook-form'

type Elements = {
  item: Form
  index: number
  register: UseFormRegister<FieldValues>
}

export const Input = ({ item, index, register }: Elements) => {
  return (
    <div className={styles.text_fiel}>
      <label htmlFor={`${item.label}${index}`} className={styles.label}>
        {index + 1}. {item.label} :
        {item.isRequired && <span className={styles.isRequired}>*</span>}
      </label>
      <input
        type={item.html_element}
        id={`${item.label}${index}`}
        placeholder="Saisissez votre réponse"
        {...register(item.label, { required: item.isRequired })}
      />
    </div>
  )
}
