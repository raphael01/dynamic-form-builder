import styles from '@/styles/loadings/LoadingPage.module.css'
import Head from 'next/head'

export function LoadingPage() {
  return (
    <>
      <Head>
        <title>Créer un formulaire - formBuildeR</title>
        <meta name="description" content="Créer un formulaire - formBuildeR" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div className={styles.container}>
        <div>
          <div className={styles.loader}>
            <div className={styles.shape1}></div>
            <div className={styles.shape2}></div>
            <div className={styles.shape3}></div>
            <div className={styles.shape4}></div>
          </div>
        </div>
      </div>
    </>
  )
}
