import React from 'react'
import styles from '@/styles/createForm/RightBlock.module.css'
import { EditForm } from './contents/EditForm'
import { useAppDispatch, useAppSelector } from '@/state/hooks'
import { cleareForm } from '@/state/formSlice'
import Swal from 'sweetalert2'
import { Toast } from '@/hooks/useInfosMessage'
import { AiOutlineLeft, AiOutlineEye } from 'react-icons/ai'
import Link from 'next/link'

type compType = {
  toggleBtn: boolean
  setToggleBtn: React.Dispatch<React.SetStateAction<boolean>>
}

export const RightBlock = ({ toggleBtn, setToggleBtn }: compType) => {
  const dataForm = useAppSelector((state) => state.formReducer)

  const dispatch = useAppDispatch()

  const resetForm = () => {
    Swal.fire({
      title: 'Supprimer le formulaire',
      text: 'Une fois le formulaire supprimé, vous ne pourrez plus le récupérer.',
      showCancelButton: true,
      confirmButtonText: 'Supprimer le formulaire',
      cancelButtonText: 'Retour',
      confirmButtonColor: '#d33',
      cancelButtonColor: '#3085d6',
      reverseButtons: true,
    }).then((result) => {
      if (result.isConfirmed) {
        dispatch(cleareForm())
      }
    })
  }

  const handleSubmit = () => {
    Toast.fire({
      icon: 'success',
      title: 'Formulaire enrégistrer avec succès.',
    })
  }
  return (
    <div
      className={
        toggleBtn ? `${styles.container} ${styles.active}` : styles.container
      }
    >
      <div className={styles.block}>
        <div className={styles.head}>
          <button onClick={() => setToggleBtn(false)}>
            <AiOutlineLeft className={styles.icon_close} />
          </button>
          {dataForm.form.length > 0 && (
            <Link href="view-form" target="_blank" className={styles.view_form}>
              <AiOutlineEye className={styles.icon} />
            </Link>
          )}
        </div>
        <EditForm />
      </div>
      <div className={styles.btn_block}>
        <button
          onClick={resetForm}
          disabled={dataForm.form.length < 1}
          className={dataForm.form.length < 1 ? styles.isDisable : ''}
        >
          Supprimer
        </button>
        <button
          onClick={handleSubmit}
          disabled={dataForm.form.length < 1}
          className={dataForm.form.length < 1 ? styles.isDisable : ''}
        >
          Enrégistrer
        </button>
      </div>
    </div>
  )
}
