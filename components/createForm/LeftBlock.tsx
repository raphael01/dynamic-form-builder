import styles from '@/styles/createForm/LeftBlock.module.css'
import {
  AiOutlineFontColors,
  AiOutlineMail,
  AiOutlineGlobal,
  AiOutlineDownCircle,
  AiOutlineDownSquare,
  AiOutlineMenu,
  AiOutlineCalendar,
  AiOutlineArrowRight,
} from 'react-icons/ai'
import { useAppDispatch } from '@/state/hooks'
import { addToform } from '@/state/formSlice'
import { uid } from 'uid'

export const LeftBlock = () => {
  const dispatch = useAppDispatch()

  const addTexFiel = (type: string) => {
    dispatch(
      addToform({
        id: uid(),
        label: `Nouveau champ (${type})`,
        name: 'create',
        html_element: type,
        isRequired: false,
      })
    )
  }

  const addRadioBtnSexe = () => {
    dispatch(
      addToform({
        id: uid(),
        label: 'Nouveau champ',
        html_element: 'radio',
        options: ['Option', 'Option'],
        isRequired: true,
      })
    )
  }

  const addCheckboxBtnCompetence = () => {
    dispatch(
      addToform({
        id: uid(),
        label: 'Nouveau champ',
        html_element: 'checkbox',
        options: ['Option'],
        isRequired: false,
      })
    )
  }

  const addSelectContrat = () => {
    dispatch(
      addToform({
        id: uid(),
        label: 'Nouveau champ',
        html_element: 'select',
        options: ['Option'],
        isRequired: false,
      })
    )
  }

  return (
    <div className={styles.container}>
      <div className={styles.block}>
        <div className={styles.head}>
          <h2>Créer un formulaire en ajoutant les composants HTML</h2>
        </div>
        <div className={styles.content}>
          <button onClick={() => addTexFiel('text')}>
            <div className={styles.content_left}>
              <AiOutlineFontColors className={styles.icon} />
              <span>Ajouter un champ de text</span>
            </div>
            <AiOutlineArrowRight className={styles.icon_right} />
          </button>

          <button onClick={() => addTexFiel('email')}>
            <div className={styles.content_left}>
              <AiOutlineMail className={styles.icon} />
              <span>Ajouter un champ de type email</span>
            </div>
            <AiOutlineArrowRight className={styles.icon_right} />
          </button>

          <button onClick={() => addTexFiel('date')}>
            <div className={styles.content_left}>
              <AiOutlineCalendar className={styles.icon} />
              <span>Ajouter un champ de type date</span>
            </div>
            <AiOutlineArrowRight className={styles.icon_right} />
          </button>

          <button onClick={() => addTexFiel('url')}>
            <div className={styles.content_left}>
              <AiOutlineGlobal className={styles.icon} />
              <span>Ajouter un champ de type url</span>
            </div>
            <AiOutlineArrowRight className={styles.icon_right} />
          </button>

          <button onClick={addRadioBtnSexe}>
            <div className={styles.content_left}>
              <AiOutlineDownCircle className={styles.icon} />
              <span>Ajouter un champ de type button radio</span>
            </div>
            <AiOutlineArrowRight className={styles.icon_right} />
          </button>

          <button onClick={addCheckboxBtnCompetence}>
            <div className={styles.content_left}>
              <AiOutlineDownSquare className={styles.icon} />
              <span>Ajouter un champ de type button checkbox</span>
            </div>
            <AiOutlineArrowRight className={styles.icon_right} />
          </button>

          <button onClick={addSelectContrat}>
            <div className={styles.content_left}>
              <AiOutlineMenu className={styles.icon} />
              <span>Ajouter un champ de type select</span>
            </div>
            <AiOutlineArrowRight className={styles.icon_right} />
          </button>
        </div>
      </div>
    </div>
  )
}
