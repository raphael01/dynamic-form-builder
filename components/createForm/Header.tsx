import styles from '@/styles/createForm/Header.module.css'
import {
  AiOutlineFileText,
  AiOutlineEye,
  AiOutlineQuestion,
} from 'react-icons/ai'
import Link from 'next/link'
import { useAppSelector } from '@/state/hooks'

export const Header = () => {
  const dataForm = useAppSelector((state) => state.formReducer)

  return (
    <div className={styles.container}>
      <div className={styles.left}>
        <AiOutlineFileText className={styles.icon} />
        <h1>formBuildeR</h1>
      </div>
      <div className={styles.center}>
        <h2>Formulaire WeaveLines</h2>
      </div>
      <div className={styles.right}>
        <div className={styles.right_block}>
          {dataForm.form.length > 0 && (
            <Link href="view-form" target="_blank" className={styles.view_form}>
              <AiOutlineEye className={styles.icon} />
              <span>Voir le formulaire</span>
            </Link>
          )}
          <Link
            href="documentation/help"
            target="_blank"
            className={styles.help}
          >
            <AiOutlineQuestion className={styles.icon} />
          </Link>
        </div>
      </div>
    </div>
  )
}
