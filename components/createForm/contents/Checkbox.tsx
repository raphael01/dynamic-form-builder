import styles from '@/styles/createForm/contents/EditForm.module.css'
import { Form } from '@/models/FormModel'
import { Header } from './components/Header'
import { Footer } from './components/Footer'
import { UpdateLabel } from './components/UpdateLabel'
import { UpdateOption } from './components/UpdateOption'
import { AddOption } from './components/AddOption'

type formElement = {
  item: Form
}

export const Checkbox = ({ item }: formElement) => {
  return (
    <div className={styles.block}>
      <Header item={item} />
      <UpdateLabel item={item} />
      {item.options?.map((el, i) => (
        <div key={i} className={styles.checbox_btn}>
          <span></span>
          <UpdateOption item={item} el={el} index={i} />
        </div>
      ))}
      <AddOption item={item} />
      <Footer item={item} />
    </div>
  )
}
