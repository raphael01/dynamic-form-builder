import React from 'react'
import styles from '@/styles/createForm/contents/EditForm.module.css'
import { TextFiel } from './TextFiel'
import { RadioBtn } from './RadioBtn'
import { Select } from './Select'
import { Checkbox } from './Checkbox'
import { useAppDispatch, useAppSelector } from '@/state/hooks'
import { updateTitle, updateDescription } from '@/state/formSlice'

export const EditForm = () => {
  const dispatch = useAppDispatch()
  const formState = useAppSelector((state) => state.formReducer)

  const updateFormTitle = (e: React.ChangeEvent<HTMLInputElement>) => {
    dispatch(updateTitle(e.target.value))
  }

  const updateFormDescript = (e: React.ChangeEvent<HTMLInputElement>) => {
    dispatch(updateDescription(e.target.value))
  }

  return (
    <div className={styles.content}>
      <div className={styles.content_block}>
        <div className={styles.title_head}>
          <input
            type="text"
            value={formState.title}
            onChange={(e) => updateFormTitle(e)}
          />
          <input
            type="text"
            value={formState.description}
            onChange={(e) => updateFormDescript(e)}
          />
        </div>
        {formState.form.map((item, i) => (
          <div key={i}>
            {item.html_element === 'text' ||
            item.html_element === 'email' ||
            item.html_element === 'url' ||
            item.html_element === 'date' ? (
              <TextFiel item={item} />
            ) : null}
            {item.html_element === 'select' && <Select item={item} />}
            {item.html_element === 'radio' && <RadioBtn item={item} />}
            {item.html_element === 'checkbox' && <Checkbox item={item} />}
          </div>
        ))}
      </div>
    </div>
  )
}
