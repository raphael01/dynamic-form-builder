import styles from '@/styles/createForm/contents/EditForm.module.css'
import { Form } from '@/models/FormModel'
import { Footer } from './components/Footer'
import { Header } from './components/Header'
import { UpdateLabel } from './components/UpdateLabel'

type formElement = {
  item: Form
}

export const TextFiel = ({ item }: formElement) => {
  return (
    <div className={styles.block}>
      <Header item={item} />
      <UpdateLabel item={item} />
      <div className={styles.text_fiel}>
        <input type={item.html_element} />
      </div>
      <Footer item={item} />
    </div>
  )
}
