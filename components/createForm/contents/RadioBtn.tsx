import styles from '@/styles/createForm/contents/EditForm.module.css'
import { Form } from '@/models/FormModel'
import { Footer } from './components/Footer'
import { Header } from './components/Header'
import { UpdateLabel } from './components/UpdateLabel'
import { AddOption } from './components/AddOption'
import { UpdateOption } from './components/UpdateOption'

type formElement = {
  item: Form
}

export const RadioBtn = ({ item }: formElement) => {
  return (
    <div className={styles.block}>
      <Header item={item} />
      <div>
        <UpdateLabel item={item} />
        {item.options?.map((el, i) => (
          <div key={i} className={styles.radio_btn}>
            <span></span>
            <UpdateOption item={item} el={el} index={i} />
          </div>
        ))}
        <AddOption item={item} />
      </div>
      <Footer item={item} />
    </div>
  )
}
