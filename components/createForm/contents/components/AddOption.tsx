import styles from '@/styles/createForm/contents/EditForm.module.css'
import { Form } from '@/models/FormModel'
import { addInformOption } from '@/state/formSlice'
import { useAppDispatch } from '@/state/hooks'

type formElement = {
  item: Form
}

export const AddOption = ({ item }: formElement) => {
  const dispatch = useAppDispatch()

  const addOption = () => {
    dispatch(
      addInformOption({
        id: item.id,
        body: 'Option',
      })
    )
  }

  return (
    <div>
      <span onClick={addOption} className={styles.add_option}>
        Ajouter une option
      </span>
    </div>
  )
}
