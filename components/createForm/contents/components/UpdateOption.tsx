import styles from '@/styles/createForm/contents/EditForm.module.css'
import { Form } from '@/models/FormModel'
import { AiOutlineDelete } from 'react-icons/ai'
import { useAppDispatch } from '@/state/hooks'
import { removeInformOption, updateOptionement } from '@/state/formSlice'

type formElement = {
  item: Form
  el: string
  index: number
}

export const UpdateOption = ({ item, el, index }: formElement) => {
  const dispatch = useAppDispatch()

  const updateOption = (
    e: React.ChangeEvent<HTMLInputElement>,
    index: number
  ) => {
    dispatch(
      updateOptionement({
        id: item.id,
        indexOption: index,
        body: e.target.value,
      })
    )
  }

  const removeOption = (index: number) => {
    dispatch(
      removeInformOption({
        id: item.id,
        indexOption: index,
      })
    )
  }
  return (
    <div className={styles.update_option}>
      <input type="text" value={el} onChange={(e) => updateOption(e, index)} />
      <AiOutlineDelete
        onClick={() => removeOption(index)}
        className={styles.icon}
      />
    </div>
  )
}
