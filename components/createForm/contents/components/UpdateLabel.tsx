import styles from '@/styles/createForm/contents/EditForm.module.css'
import { Form } from '@/models/FormModel'
import { updateFormElement } from '@/state/formSlice'
import { useDispatch } from 'react-redux'

type formElement = {
  item: Form
}

export const UpdateLabel = ({ item }: formElement) => {
  const dispatch = useDispatch()
  const updateFormEl = (e: React.ChangeEvent<HTMLInputElement>) => {
    dispatch(
      updateFormElement({
        id: item.id,
        label: e.target.value,
        html_element: item.html_element,
        isRequired: item.isRequired,
        options: item.options,
      })
    )
  }

  return (
    <div className={styles.label}>
      <input type="text" value={item.label} onChange={(e) => updateFormEl(e)} />
      {item.isRequired && <span className={styles.isRequired}>*</span>}
    </div>
  )
}
