import styles from '@/styles/createForm/contents/EditForm.module.css'
import { Form } from '@/models/FormModel'
import { toggleToform } from '@/state/formSlice'
import { useDispatch } from 'react-redux'

type formElement = {
  item: Form
}

export const Footer = ({ item }: formElement) => {
  const dispatch = useDispatch()
  const toggleButton = (e: React.ChangeEvent<HTMLInputElement>) => {
    dispatch(
      toggleToform({
        id: item.id,
        label: item.label,
        html_element: item.html_element,
        isRequired: e.target.checked,
        options: item.options,
      })
    )
  }

  return (
    <div className={styles.footer}>
      <p>Cet champ est obligatoire ?</p>
      <input
        type="checkbox"
        checked={item.isRequired}
        onChange={(e) => toggleButton(e)}
      />
    </div>
  )
}
