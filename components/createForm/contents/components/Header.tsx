import styles from '@/styles/createForm/contents/EditForm.module.css'
import { Form } from '@/models/FormModel'
import { removeToform } from '@/state/formSlice'
import { useDispatch } from 'react-redux'
import { AiOutlineDelete } from 'react-icons/ai'

type formElement = {
  item: Form
}

export const Header = ({ item }: formElement) => {
  const dispatch = useDispatch()
  const removeTexFiel = () => {
    dispatch(removeToform(item.id))
  }

  return (
    <div className={styles.head}>
      <span onClick={removeTexFiel}>
        <AiOutlineDelete className={styles.icon} />
      </span>
    </div>
  )
}
