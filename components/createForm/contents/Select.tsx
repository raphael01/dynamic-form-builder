import styles from '@/styles/createForm/contents/EditForm.module.css'
import { Form } from '@/models/FormModel'
import { Header } from './components/Header'
import { Footer } from './components/Footer'
import { UpdateLabel } from './components/UpdateLabel'
import { UpdateOption } from './components/UpdateOption'
import { AddOption } from './components/AddOption'

type formElement = {
  item: Form
}

export const Select = ({ item }: formElement) => {
  return (
    <div className={styles.block}>
      <Header item={item} />
      <UpdateLabel item={item} />
      <select>
        <option disabled defaultValue="Choisir une option">
          Choisir une option
        </option>
        {item.options?.map((el, i) => (
          <option value={el} key={i}>
            {el}
          </option>
        ))}
      </select>
      {item.options?.map((el, i) => (
        <div key={i}>
          <UpdateOption item={item} el={el} index={i} />
        </div>
      ))}
      <AddOption item={item} />
      <Footer item={item} />
    </div>
  )
}
