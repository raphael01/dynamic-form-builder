export type Form = {
  id: string
  label: string
  html_element: string
  isRequired: boolean
  options?: string[]
}

export type FormType = {
  title: string
  description: string
  form: Form[]
}
